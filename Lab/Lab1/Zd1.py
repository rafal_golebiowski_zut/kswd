import numpy as np
from Lab1 import simplex

A = np.array([[4, 1, -1, 0, 0, 0, 0, 1, 0, 0],
              [1, 1, 0, -1, 0, 0, 0, 0, 1, 0],
              [1, 3, 0, 0, -1, 0, 0, 0, 0, 1],
              [1, -1, 0, 0, 0, 1, 0, 0, 0, 0],
              [3, 10, 0, 0, 0, 0, 1, 0, 0, 0]]).astype(float)

B = np.array([[12],
              [6],
              [10],
              [6],
              [83]]).astype(float)

M = 1000
C = np.array([5, 1, 0, 0, 0, 0, 0, M, M, M]).astype(float)

Cb = np.array([M, M, M, 0, 0]).astype(float)



print("min = ", simplex(A, B, C, Cb))






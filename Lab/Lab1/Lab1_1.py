import numpy as np
from Lab1 import simplex

A = np.array([[20, 4, 1, 0],
              [6, 2, 0, 1]]).astype(float)

B = np.array([[80],
              [68]]).astype(float)

C = np.array([-3, -10, 0, 0]).astype(float)

Cb = np.array([0, 0]).astype(float)



print("max = ", -simplex(A, B, C, Cb))
import numpy as np


def simplex(A, B, C, Cb):
    while(True):
        print("A = \n", A)
        print("B = \n", B)
        print("Cb = \n", Cb, "\n")
    
        Z = Cb.dot(A)
        delta = C - Z
        
        print("Z = \n", Z)
        print("delta = \n", delta, "\n\n")
        
        if(not (delta < 0).any()):
            break
        
        minZIndex = np.argmin(delta)
        
        
        theta = B[:, 0] / A[:, minZIndex]
        minTheta = None
        for index, val in enumerate(theta):
            if(minTheta is None or (val >= 0 and val < minTheta)):
                if(val == 0 and A[index, minZIndex] < 0):
                    continue
                minTheta = val
        if(minTheta is None):
            print("Error: Theta")
            break
        minThetaIndex = np.where(theta == minTheta)[0][0]
        
        
        tmp = A[minThetaIndex, minZIndex];
        A[minThetaIndex, :] /= tmp;
        B[minThetaIndex, :] /= tmp;
        
        for i in range(B.size):
            if(i != minThetaIndex):
                B[i, :] -= B[minThetaIndex, :] * (A[i, minZIndex])
                A[i, :] -= A[minThetaIndex, :] * (A[i, minZIndex])
                
        Cb[minThetaIndex] = C[minZIndex]
        
    return Cb.dot(B)[0]





if __name__ == "__main__":
    A = np.array([[2, 4, 1, 0],
              [5, 2, 0, 1]]).astype(float)

    B = np.array([[40],
                  [40]]).astype(float)
    
    C = np.array([-3, -2, 0, 0]).astype(float)
    
    Cb = np.zeros(2)
    print("A = \n", A)
    print("B = \n", B)
    print("C = \n", C, "\n")
    print("max = ", -simplex(A, B, C, Cb))






import numpy as np

from Promethee import promethee
from Promethee import vShapePreferenceFunction
from Promethee import CriteriumType

attributeMatrix = np.array([
    [21000, 2500, 9800, 167.5, 0, 0, 0],
    [21000, 4000, 10100, 161.2, 3, 3, 2],
    [21000, 7000, 11400, 83.8, 3, 3, 2],
    [86000, 20000, 22000, 95.4, 1, 1, 1],
    [29000, 4000, 15000, 158.2, 1, 0, 0],
    [59000, 7000, 15000, 105.8, 1, 0, 0],
    [59000, 17500, 15000, 72.9, 1, 1, 1]
])

weights = np.array([0.21, 0.14, 0.35, 0.075, 0.075, 0.075, 0.075])
criteriumTypes = [CriteriumType.COST, CriteriumType.COST, CriteriumType.COST,
                  CriteriumType.COST, CriteriumType.BENEFIT, CriteriumType.BENEFIT, CriteriumType.BENEFIT]

preferenceFunctions = [lambda d: vShapePreferenceFunction(d, 15000, 5000),
                        lambda d: vShapePreferenceFunction(d, 4000, 1000),
                        lambda d: vShapePreferenceFunction(d, 2500, 500),
                        lambda d: vShapePreferenceFunction(d, 30, 10),
                        lambda d: vShapePreferenceFunction(d, 2, 0),
                        lambda d: vShapePreferenceFunction(d, 2, 0),
                        lambda d: vShapePreferenceFunction(d, 2, 0)]

print(promethee(attributeMatrix, weights, criteriumTypes, preferenceFunctions))
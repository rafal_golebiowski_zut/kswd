from tkinter import *
from PrometheeGUI import SimpleTableInput

class MainMenu(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)


        # register a command to use for validation
        vcmd = (self.register(self._validate), "%P")

        label = Label(self, text = "CriteriumCount")
        label.grid(row = 0, column = 0)
        self.criteriumEntry = Entry(self, validate="key", validatecommand=vcmd)
        self.criteriumEntry.insert(END, 4)
        self.criteriumEntry.grid(row = 0, column = 1)

        label = Label(self, text = "AlternativesCount")
        label.grid(row = 1, column = 0)
        self.alternativeEntry = Entry(self, validate="key", validatecommand=vcmd)
        self.alternativeEntry.insert(END, 4)
        self.alternativeEntry.grid(row = 1, column = 1)

        submitButton = Button(self, text = "Submit", command = self.onSubmit)
        submitButton.grid(row = 2, column = 0, columnspan = 2)


    def _validate(self, P):
        '''Perform input validation. 

        Allow only an empty value, or a value that can be converted to a int
        '''
        if P.strip() == "":
            return True

        try:
            f = int(P)
        except ValueError:
            self.bell()
            return False
        return True

    def onSubmit(self):
        try:
            alternativeCount = int(self.alternativeEntry.get())
            criteriumCount = int(self.criteriumEntry.get())
        except ValueError:
            return
        if(alternativeCount < 1):
            return
        if(criteriumCount < 1):
            return

        print(self.alternativeEntry.get())
        print(alternativeCount)
        print(self.criteriumEntry.get())
        print(criteriumCount)
        dialog = Toplevel()
        dialog.grab_set()
        table = SimpleTableInput(dialog, alternativeCount, criteriumCount)
        table.pack(side="top", fill="both", expand=True)


root = Tk()
MainMenu(root).pack(side="top", fill="both", expand=True)
root.mainloop()
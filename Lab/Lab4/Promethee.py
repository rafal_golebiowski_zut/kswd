import numpy as np
from enum import Enum

class CriteriumType(Enum):
    BENEFIT = 1
    COST = 2


def promethee(attributeMatrix, weights, criteriumTypes, preferenceFunctions):
    
    rows = attributeMatrix.shape[0]
    cols = attributeMatrix.shape[1]
    fi = np.zeros_like(attributeMatrix)
    for k in range(cols):
        d = np.repeat(attributeMatrix[:, k][:, np.newaxis], rows, 1)
        d -= d.T

        P = preferenceFunctions[k](d)
        if(criteriumTypes[k] == CriteriumType.COST):
            P = P.T
        
        fiPlus = P.sum(axis = 1) / (rows - 1)
        fiMinus = P.sum(axis = 0) / (rows - 1)

        fi[:, k] = fiPlus - fiMinus

    fi = np.sum(fi * weights, axis = 1)

    return fi


def usualPreferenceFunction(d):
    return (d > 0).astype(float)

def uShapePreferenceFunction(d, q):
    return (d > q).astype(float)

def vShapePreferenceFunction(d, p, q = 0):
    dCopy = (d > q).astype(float)
    dCopy[(d > q) * (d < p)] = (d[(d > q) * (d < p)] - q)/(p - q)

    return dCopy

def levelPreferenceFunction(d, p, q):
    dCopy = (d > q).astype(float)
    dCopy[(d > q) * (d < p)] = 0.5

    return dCopy

def gaussianPreferenceFunction(d, s):
    dCopy = np.zeros_like(d).astype(float)
    dCopy[(d > 0)] = 1 - np.exp(-(d[(d > 0)]**2) / (2 * s**2))

    return dCopy


from tkinter import *
import tkinter as tk
import numpy as np
import tkinter.ttk as ttk


from Promethee import promethee
from Promethee import usualPreferenceFunction
from Promethee import uShapePreferenceFunction
from Promethee import vShapePreferenceFunction
from Promethee import levelPreferenceFunction
from Promethee import gaussianPreferenceFunction
from Promethee import CriteriumType

class SimpleTableInput(tk.Frame):
    def __init__(self, parent, rows, columns, headings = None, rowHeadings = None):
        tk.Frame.__init__(self, parent)

        self._entry = {}
        self.rows = rows
        self.columns = columns

        self.weightEntries = []
        self.weightTypeComboboxes = []
        self.preferenceFunctionsComboboxes = []
        self.preferencePEntries = []
        self.preferenceQEntries = []

        if(headings == None or len(headings) != columns):
            self.headings = [f'C{i + 1}' for i in range(columns)]
        else:
            self.headings = headings

        if(rowHeadings == None or len(rowHeadings) != rows):
            self.rowHeadings = [f'A{i + 1}' for i in range(rows)]
        else:
            self.rowHeadings = rowHeadings

        # register a command to use for validation
        vcmd = (self.register(self._validate), "%P")

        for column in range(self.columns):
            e = tk.Entry(self)
            e.insert(END, self.headings[column])
            e.grid(row=0, column = column + 1, stick="nsew")
        
        # create the table of widgets
        for row in range(self.rows):
            e = tk.Entry(self, validate="key")
            e.insert(END, self.rowHeadings[row])
            e.grid(row=row + 1, column = 0, stick="nsew")

            for column in range(self.columns):
                index = (row, column)
                e = tk.Entry(self, validate="key", validatecommand=vcmd)
                e.insert(END, np.random.rand())
                e.grid(row=row + 1, column=column + 1, stick="nsew")
                self._entry[index] = e
        
        # weight row
        l = Label(self, text = "W")
        l.grid(row = rows + 1, column = 0)
        for column in range(self.columns):
            e = tk.Entry(self)
            e.insert(END, 1/self.columns)
            e.grid(row= rows + 1, column = column + 1, stick="nsew")
            self.weightEntries.append(e)

        # weight type row
        l = Label(self, text = "W type")
        l.grid(row = rows + 2, column = 0)
        for column in range(self.columns):
            comboBox = ttk.Combobox(self, state="readonly", values = ["B", "C"])
            comboBox.current(0)
            comboBox.grid(row= rows + 2, column = column + 1, stick="nsew")
            self.weightTypeComboboxes.append(comboBox)

        l = Label(self, text = "Preference function")
        l.grid(row = rows + 3, column = 0)
        for column in range(self.columns):
            comboBox = ttk.Combobox(self, state="readonly", values = ["Usual", "U-shape", "V-shape", "Level", "V-shape with q", "Gaussian"])
            comboBox.current(0)
            comboBox.grid(row= rows + 3, column = column + 1, stick="nsew")
            self.preferenceFunctionsComboboxes.append(comboBox)

        l = Label(self, text = "q or s")
        l.grid(row = rows + 4, column = 0)
        for column in range(self.columns):
            e = tk.Entry(self, validate="key")
            e.insert(END, 0)
            e.grid(row= rows + 4, column = column + 1, stick="nsew")
            self.preferenceQEntries.append(e)

        l = Label(self, text = "p")
        l.grid(row = rows + 5, column = 0)
        for column in range(self.columns):
            e = tk.Entry(self, validate="key")
            e.insert(END, 0)
            e.grid(row= rows + 5, column = column + 1, stick="nsew")
            self.preferencePEntries.append(e)




        button = Button(self, text = "Submit", command = self.onSubmit)
        button.grid(row = rows + 6, column = 0, columnspan = self.columns + 1)
        
        # adjust column weights so they all expand equally
        for column in range(self.columns + 1):
            self.grid_columnconfigure(column, weight=1)
        # designate a final, empty row to fill up any extra space
        #self.grid_rowconfigure(rows + 1, weight=1)


    def get(self):
        '''Return a list of lists, containing the data in the table'''
        result = []
        for row in range(self.rows):
            current_row = []
            for column in range(self.columns):
                index = (row, column)
                current_row.append(self._entry[index].get())
            result.append(current_row)
        return np.array(result).astype(float)

    def getWeights(self):
        result = []
        for column in range(self.columns):
            result.append(self.weightEntries[column].get())
        return np.array(result).astype(float)

    def getWeightTypes(self):
        result = []
        for column in range(self.columns):
            result.append(CriteriumType(self.weightTypeComboboxes[column].current() + 1))
        return result

    def getPreferenceFunctions(self):
        result = []
        for column in range(self.columns):
            selectedFunc = self.preferenceFunctionsComboboxes[column].current()
            if(selectedFunc == 0):
                result.append(usualPreferenceFunction)
            elif(selectedFunc == 1):
                result.append(lambda d: uShapePreferenceFunction(d, float(self.preferenceQEntries[column].get())))
            elif(selectedFunc == 2):
                result.append(lambda d: vShapePreferenceFunction(d, float(self.preferencePEntries[column].get())))
            elif(selectedFunc == 3):
                result.append(lambda d: levelPreferenceFunction(d, float(self.preferencePEntries[column].get()), float(self.preferenceQEntries[column].get())))
            elif(selectedFunc == 5):
                result.append(lambda d: vShapePreferenceFunction(d, float(self.preferencePEntries[column].get()), float(self.preferenceQEntries[column].get())))
            else: #if(selectedFunc == 6):
                result.append(lambda d: gaussianPreferenceFunction(d, float(self.preferenceQEntries[column].get())))
        return result

    def _validate(self, P):
        '''Perform input validation. 

        Allow only an empty value, or a value that can be converted to a float
        '''
        if P.strip() == "":
            return True

        try:
            f = float(P)
        except ValueError:
            self.bell()
            return False
        return True

    def onSubmit(self):
        print(self.get())
        print(self.getWeights())
        print(self.getWeightTypes())
        print(self.getPreferenceFunctions())

        result = promethee(self.get(), self.getWeights(), self.getWeightTypes(), self.getPreferenceFunctions())
        print(result)
        dialog = Toplevel()
        dialog.grab_set()
        table = PrometheeResultView(dialog, result)
        table.pack(side="top", fill="both", expand=True)


class PrometheeResultView(tk.Frame):
    def __init__(self, parent, prometheeResults):
        tk.Frame.__init__(self, parent)

        self.rows = len(prometheeResults)

        self.rowHeadings = [f'A{i + 1}' for i in range(self.rows)]


        e = tk.Entry(self)
        e.insert(END, 'fi')
        e.grid(row=0, column = 1, stick="nsew")
        
        # create the table of widgets
        for row in range(self.rows):
            e = tk.Entry(self, validate="key")
            e.insert(END, self.rowHeadings[row])
            e.grid(row=row + 1, column = 0, stick="nsew")

            e = tk.Entry(self, validate="key")
            e.insert(END, prometheeResults[row])
            e.grid(row=row + 1, column= 1, stick="nsew")
        
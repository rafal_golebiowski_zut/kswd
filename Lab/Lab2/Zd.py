import numpy as np
from matplotlib import pyplot as plt

from Topsis import topsis
from Topsis import CriteriumType
from NormalizeFunctions import normalizeType1
from NormalizeFunctions import normalizeType2
from NormalizeFunctions import normalizeType3
from NormalizeFunctions import normalizeType4


N = 10
M = 100
loops = 1000

weights = np.random.random(N)
weights = weights / np.sum(weights)

criteriumTypes = [CriteriumType(value) for _, value in enumerate(np.random.randint(1, 3, N))]
normalizeFunctions = [normalizeType1, normalizeType2, normalizeType3, normalizeType4]
standardDeviationsPercents = np.arange(0.2, 1.1, 0.2)[::-1]

y = np.zeros((len(standardDeviationsPercents), len(normalizeFunctions), M + 1, len(normalizeFunctions)))
C = np.zeros((len(normalizeFunctions), M))
randomMids = np.array([4**x for x in range(N)])

for i, standardDeviationsPercent in enumerate(standardDeviationsPercents):
    for _ in range(loops):
        attributeMatrix = np.random.normal(randomMids, standardDeviationsPercent * randomMids, (M, N))
        
        for k in range(3, M + 1):
            for j, normalizeFunction in enumerate(normalizeFunctions):
                C[j, :k] = topsis(attributeMatrix[:k, :], weights, criteriumTypes, normalizeFunction)
            
            for j in range(len(normalizeFunctions)):
                for jj in range(len(normalizeFunctions)):
                    y[i, j, k, jj] += np.mean(np.abs(C[j, :k] - C[jj, :k]))

y /= loops


legend = np.array(["(x-min)/(max-min)", "x/max", "x/sum(x)", "x/sqrt(sum(x))"])
for j in range(len(normalizeFunctions)):
    for jj in range(len(normalizeFunctions)):
        if(j != jj):
            for i in range(len(standardDeviationsPercents)):
                plt.plot(range(3, M + 1), y[i, j, 3:M + 1, jj], label = "{0:.1f}".format(standardDeviationsPercents[i]))
            plt.legend()
            plt.title(f'{legend[j]} vs {legend[jj]}')
            #plt.savefig(f'MyExperiment_{j + 1}vs{jj + 1}.png', dpi = 300)
            #plt.figure()
            plt.show()


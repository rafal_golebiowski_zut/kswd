import numpy as np
from enum import Enum

class CriteriumType(Enum):
    BENEFIT = 1
    COST = 2
    NONPARAMETRIC = 3


def topsis(attributeMatrix, weights, criteriumTypes, normalizeFunction):
    normalized = normalizeFunction(attributeMatrix)
    weighted = normalized * weights

    PIS = []
    NIS = []

    for i, col in enumerate(weighted.T):
        if(criteriumTypes[i] == CriteriumType.BENEFIT):
            PIS.append(np.max(col))
            NIS.append(np.min(col))
        elif(criteriumTypes[i] == CriteriumType.COST):
            PIS.append(np.min(col))
            NIS.append(np.max(col))
        else: #if(criteriumTypes[i] == CriteriumType.NONPARAMETRIC):
            PIS.append(0)
            NIS.append(0)
            weighted[:, i] = np.zeros_like(col)

    PIS = np.array(PIS)
    NIS = np.array(NIS)

    DPlus = np.sum((weighted - PIS)**2, axis = 1)**0.5
    DMinus = np.sum((weighted - NIS)**2, axis = 1)**0.5

    C = DMinus / (DPlus + DMinus)

    return C


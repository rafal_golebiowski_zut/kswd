import numpy as np
from matplotlib import pyplot as plt

from Topsis import topsis
from Topsis import CriteriumType
from NormalizeFunctions import normalizeType1
from NormalizeFunctions import normalizeType2
from NormalizeFunctions import normalizeType3
from NormalizeFunctions import normalizeType4


N = 10
M = 100
loops = 1000

weights = np.random.random(N)
weights = weights / np.sum(weights)

criteriumTypes = [CriteriumType(value) for _, value in enumerate(np.random.randint(1, 3, N))]
normalizeFunctions = [normalizeType1, normalizeType2, normalizeType3, normalizeType4]

y = np.zeros((len(normalizeFunctions), M + 1, len(normalizeFunctions)))
C = np.zeros((len(normalizeFunctions), M))
baseRandomMaxs = np.array([4**x for x in range(N)])

for _ in range(loops):
    randomMaxs = np.random.uniform(baseRandomMaxs / 2, baseRandomMaxs, N).astype(int) + 1
    randomMins = np.random.uniform(high = randomMaxs, size = N).astype(int)
    attributeMatrix = np.random.uniform(randomMins, randomMaxs, (M, N))
    
    for k in range(3, M + 1):
        for j, normalizeFunction in enumerate(normalizeFunctions):
            C[j, :k] = topsis(attributeMatrix[:k, :], weights, criteriumTypes, normalizeFunction)
        
        for j in range(len(normalizeFunctions)):
            for jj in range(len(normalizeFunctions)):
                y[j, k, jj] += np.mean(np.abs(C[j, :k] - C[jj, :k]))

y /= loops


legend = np.array(["(x-min)/(max-min)", "x/max", "x/sum(x)", "x/sqrt(sum(x))"])
colors = np.array(["b", "darkorange", "g", "r"])
for j in range(len(normalizeFunctions)):
    for jj in range(len(normalizeFunctions)):
        if(j != jj):
            plt.plot(range(3, M + 1), y[j, 3:M + 1, jj], colors[jj])
    
    plt.legend(np.delete(legend, j))
    plt.show()

for j in range(len(normalizeFunctions)):
    for jj in range(len(normalizeFunctions)):
        plt.plot(range(3, M + 1), y[j, 3:M + 1, jj], colors[jj])
    
    plt.legend(legend)
    plt.show()

import numpy as np
from enum import Enum
import itertools

class CriteriumType(Enum):
    BENEFIT = 1
    COST = 2


def vikor(attributeMatrix, weights, criteriumTypes, normalizeFunction, v):
    normalized = normalizeFunction(attributeMatrix)

    fPLus = []
    fMinus = []

    for i, col in enumerate(normalized.T):
        if(criteriumTypes[i] == CriteriumType.BENEFIT):
            fPLus.append(np.max(col))
            fMinus.append(np.min(col))
        else: #if(criteriumTypes[i] == CriteriumType.COST):
            fPLus.append(np.min(col))
            fMinus.append(np.max(col))
    
    fPLus = np.array(fPLus)
    fMinus = np.array(fMinus)

    tempMatrix = weights * ((fPLus - normalized) / (fPLus - fMinus))

    S = np.sum(tempMatrix, axis = 1)
    R = np.max(tempMatrix, axis = 1)

    SPlus = np.min(S)
    SMinus = np.max(S)
    RPlus = np.min(R)
    RMinus = np.max(R)

    Q = v * (S - SPlus)/(SMinus - SPlus) + (1 - v)*(R - RPlus)/(RMinus - RPlus)

    QOrder = np.argsort(Q)
    DQ = 1/(attributeMatrix.shape[0] - 1)
    if(Q[QOrder[1]] - Q[QOrder[0]] >= DQ):
        if(QOrder[0] == np.argmin(S) or QOrder[0] == np.argmin(R)):
            compromiseSolution = np.array([QOrder[0]])
        else:
            compromiseSolution = np.array([QOrder[0], QOrder[1]])
    else:
        compromiseSolution = np.array(list(itertools.takewhile(lambda x: Q[x] - Q[QOrder[0]] < DQ, QOrder)))

    return S, R, Q, compromiseSolution


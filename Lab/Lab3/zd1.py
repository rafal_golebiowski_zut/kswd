import numpy as np
from matplotlib import pyplot as plt

from Vikor import vikor
from Vikor import CriteriumType
from NormalizeFunctions import noNormalize
from NormalizeFunctions import normalizeType1
from NormalizeFunctions import normalizeType2
from NormalizeFunctions import normalizeType3
from NormalizeFunctions import normalizeType4


N = 10
M = 100
loops = 2

weights = np.random.random(N)
weights = weights / np.sum(weights)

v = np.linspace(0, 1, 11)

criteriumTypes = [CriteriumType(value) for _, value in enumerate(np.random.randint(1, 3, N))]
normalizeFunctions = [noNormalize, normalizeType1, normalizeType2, normalizeType3, normalizeType4]

attributeMatrix = np.zeros((2, M, N))
S = np.zeros((attributeMatrix.shape[0], len(v), len(normalizeFunctions), M))
R = np.zeros((attributeMatrix.shape[0], len(v), len(normalizeFunctions), M))
Q = np.zeros((attributeMatrix.shape[0], len(v), len(normalizeFunctions), M))
# 1. Attribute matrix - 2
# 2. v - 0-1
# 3. S, R, Q - 3
# 4. normalizeFunction vs
# 5. M - 3-100
# 6.                   vs normalizeFunction 
y = np.zeros((attributeMatrix.shape[0], len(v), 3, len(normalizeFunctions), M + 1, len(normalizeFunctions)))
yAttributeMatrix = np.zeros((len(normalizeFunctions), len(v), 3, attributeMatrix.shape[0], M + 1, attributeMatrix.shape[0]))

randomMaxs = np.array([4**x for x in range(N)])
    
for _ in range(loops):
    attributeMatrix[0, :, :] = np.random.uniform(np.zeros_like(randomMaxs), randomMaxs, (M, N))
    attributeMatrix[1, :, :] = np.maximum(np.random.normal(0.5 * randomMaxs, 0.5 * randomMaxs, (M, N)), 0)
    
    for k in range(3, M + 1):
        for j, normalizeFunction in enumerate(normalizeFunctions):
            for vIndex, vValue in enumerate(v):
                for attributeMatrixIndex in range(attributeMatrix.shape[0]):
                    S[attributeMatrixIndex, vIndex, j, :k], R[attributeMatrixIndex, vIndex, j, :k], Q[attributeMatrixIndex, vIndex, j, :k], _ = vikor(attributeMatrix[attributeMatrixIndex, :k, :], weights, criteriumTypes, normalizeFunction, vValue)
            
        for j in range(len(normalizeFunctions)):
            for jj in range(len(normalizeFunctions)):
                for vIndex, vValue in enumerate(v):
                    for attributeMatrixIndex in range(attributeMatrix.shape[0]):
                        y[attributeMatrixIndex, vIndex, 0, j, k, jj] += np.mean(np.abs(S[attributeMatrixIndex, vIndex, j, :k] - S[attributeMatrixIndex, vIndex, jj, :k]))
                        y[attributeMatrixIndex, vIndex, 1, j, k, jj] += np.mean(np.abs(R[attributeMatrixIndex, vIndex, j, :k] - R[attributeMatrixIndex, vIndex, jj, :k]))
                        y[attributeMatrixIndex, vIndex, 2, j, k, jj] += np.mean(np.abs(Q[attributeMatrixIndex, vIndex, j, :k] - Q[attributeMatrixIndex, vIndex, jj, :k]))

        for attributeMatrixIndex1 in range(attributeMatrix.shape[0]):
            for attributeMatrixIndex2 in range(attributeMatrix.shape[0]):
                for vIndex, vValue in enumerate(v):
                    for j in range(len(normalizeFunctions)):
                        yAttributeMatrix[j, vIndex, 0, attributeMatrixIndex1, k, attributeMatrixIndex2] += np.mean(np.abs(S[attributeMatrixIndex1, vIndex, j, :k] - S[attributeMatrixIndex2, vIndex, j, :k]))
                        yAttributeMatrix[j, vIndex, 1, attributeMatrixIndex1, k, attributeMatrixIndex2] += np.mean(np.abs(R[attributeMatrixIndex1, vIndex, j, :k] - R[attributeMatrixIndex2, vIndex, j, :k]))
                        yAttributeMatrix[j, vIndex, 2, attributeMatrixIndex1, k, attributeMatrixIndex2] += np.mean(np.abs(Q[attributeMatrixIndex1, vIndex, j, :k] - Q[attributeMatrixIndex2, vIndex, j, :k]))


y /= loops

normalizeFunctionNames = ["noNormalize", "(x-min)/(max-min)", "x/max", "x/sum(x)", "x/sqrt(sum(x))"]
attributeMatrixNames = ["uniform", "normal"]


# Różnice między S dla norm[1..5] i attributeMatrix[1..2]
for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for j in range(len(normalizeFunctions)):
        for jj in range(len(normalizeFunctions)):
            plt.plot(range(3, M + 1), y[attributeMatrixIndex, 0, 0, j, 3:M + 1, jj])
        plt.legend(normalizeFunctionNames)
        plt.title(f'SDifference_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}')
        plt.savefig(f'SDifference_{attributeMatrixNames[attributeMatrixIndex]}_{j}.png', dpi = 300)
        plt.figure()

for j in range(len(normalizeFunctions)):
    for attributeMatrixIndex1 in range(attributeMatrix.shape[0]): 
        for attributeMatrixIndex2 in range(attributeMatrix.shape[0]): 
            plt.plot(range(3, M + 1), yAttributeMatrix[j, 0, 0, attributeMatrixIndex1, 3:M + 1, attributeMatrixIndex2])
        plt.legend(attributeMatrixNames)
        plt.title(f'SDifference_{normalizeFunctionNames[j]}')
        plt.savefig(f'SDifference_{j}.png', dpi = 300)
        plt.figure()

for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for j in range(len(normalizeFunctions)):
        for jj in range(len(normalizeFunctions)):
            plt.plot(range(3, M + 1), y[attributeMatrixIndex, 0, 1, j, 3:M + 1, jj])
        plt.legend(normalizeFunctionNames)
        plt.title(f'RDifference_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}')
        plt.savefig(f'RDifference_{attributeMatrixNames[attributeMatrixIndex]}_{j}.png', dpi = 300)
        plt.figure()

for j in range(len(normalizeFunctions)):
    for attributeMatrixIndex1 in range(attributeMatrix.shape[0]): 
        for attributeMatrixIndex2 in range(attributeMatrix.shape[0]): 
            plt.plot(range(3, M + 1), yAttributeMatrix[j, 0, 1, attributeMatrixIndex1, 3:M + 1, attributeMatrixIndex2])
        plt.legend(attributeMatrixNames)
        plt.title(f'RDifference_{normalizeFunctionNames[j]}')
        plt.savefig(f'RDifference_{j}.png', dpi = 300)
        plt.figure()


# for j in range(len(normalizeFunctions)):
#     for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
#         for vIndex, vValue in enumerate(v):
#             plt.plot(range(M), S[attributeMatrixIndex, vIndex, j, :])
#             plt.plot(range(M), R[attributeMatrixIndex, vIndex, j, :])
#             plt.plot(range(M), Q[attributeMatrixIndex, vIndex, j, :])
#             plt.legend(["S", "R", "Q"])
#             plt.title(f'SRQ_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}_{vIndex}')
#             plt.savefig(f'SRQ_{attributeMatrixNames[attributeMatrixIndex]}_{j}_{vIndex}.png', dpi = 300)
#             plt.figure()
#             # plt.show()
        

# for j in range(len(normalizeFunctions)):
#     for jj in range(len(normalizeFunctions)):
#         plt.plot(range(3, M + 1), y[0, 5, 2, j, 3:M + 1, jj])
    
#     plt.legend(["(x-min)/(max-min)", "x/max", "x/sum(x)", "x/sqrt(sum(x))"])
#     plt.show()



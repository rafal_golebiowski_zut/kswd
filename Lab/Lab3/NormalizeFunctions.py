import numpy as np

def noNormalize(mat):
    return mat

def normalizeType1(mat):
    mins = np.min(mat, axis = 0)
    maxs = np.max(mat, axis = 0)
    return (mat - mins) / (maxs - mins)

def normalizeType2(mat):
    return mat / np.max(mat, axis = 0)
    
def normalizeType3(mat):
    return mat / np.sum(mat, axis = 0)
    
def normalizeType4(mat):
    return mat / np.sum(mat, axis = 0)**0.5
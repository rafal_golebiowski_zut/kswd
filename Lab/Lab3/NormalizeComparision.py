import numpy as np
from matplotlib import pyplot as plt

from Vikor import vikor
from Vikor import CriteriumType
from NormalizeFunctions import noNormalize
from NormalizeFunctions import normalizeType1
from NormalizeFunctions import normalizeType2
from NormalizeFunctions import normalizeType3
from NormalizeFunctions import normalizeType4


N = 10
M = 100
loops = 1000

weights = np.random.random(N)
weights = weights / np.sum(weights)

v = 0.5

criteriumTypes = [CriteriumType(value) for _, value in enumerate(np.random.randint(1, 3, N))]
normalizeFunctions = [noNormalize, normalizeType1, normalizeType2, normalizeType3, normalizeType4]

attributeMatrix = np.zeros((2, M, N))
S = np.zeros((attributeMatrix.shape[0], len(normalizeFunctions), M))
R = np.zeros((attributeMatrix.shape[0], len(normalizeFunctions), M))
Q = np.zeros((attributeMatrix.shape[0], len(normalizeFunctions), M))
# 1. Attribute matrix - 2
# 2. S, R, Q - 3
# 3. normalizeFunction vs
# 4. M - 3-100
# 5.                   vs normalizeFunction 
y = np.zeros((attributeMatrix.shape[0], 3, len(normalizeFunctions), M + 1, len(normalizeFunctions)))

randomMaxs = np.array([4**x for x in range(N)])
    
for _ in range(loops):
    attributeMatrix[0, :, :] = np.random.uniform(np.zeros_like(randomMaxs), randomMaxs, (M, N))
    attributeMatrix[1, :, :] = np.maximum(np.random.normal(0.5 * randomMaxs, 0.5 * randomMaxs, (M, N)), 0)
    
    for k in range(3, M + 1):
        for j, normalizeFunction in enumerate(normalizeFunctions):
            for attributeMatrixIndex in range(attributeMatrix.shape[0]):
                S[attributeMatrixIndex, j, :k], R[attributeMatrixIndex, j, :k], Q[attributeMatrixIndex, j, :k], _ = vikor(attributeMatrix[attributeMatrixIndex, :k, :], weights, criteriumTypes, normalizeFunction, v)
            
        for j in range(len(normalizeFunctions)):
            for jj in range(len(normalizeFunctions)):
                for attributeMatrixIndex in range(attributeMatrix.shape[0]):
                    y[attributeMatrixIndex, 0, j, k, jj] += np.mean(np.abs(S[attributeMatrixIndex, j, :k] - S[attributeMatrixIndex, jj, :k]))
                    y[attributeMatrixIndex, 1, j, k, jj] += np.mean(np.abs(R[attributeMatrixIndex, j, :k] - R[attributeMatrixIndex, jj, :k]))
                    y[attributeMatrixIndex, 2, j, k, jj] += np.mean(np.abs(Q[attributeMatrixIndex, j, :k] - Q[attributeMatrixIndex, jj, :k]))

y /= loops

normalizeFunctionNames = ["noNormalize", "(x-min)/(max-min)", "x/max", "x/sum(x)", "x/sqrt(sum(x))"]
attributeMatrixNames = ["uniform", "normal"]


# Różnice między S dla norm[1..5] i attributeMatrix[1..2]
for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for j in range(len(normalizeFunctions)):
        for jj in range(len(normalizeFunctions)):
            plt.plot(range(3, M + 1), y[attributeMatrixIndex, 0, j, 3:M + 1, jj])
        plt.legend(normalizeFunctionNames)
        plt.title(f'SDifference_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}')
        plt.savefig(f'normalizeComparision/SDifference_{attributeMatrixNames[attributeMatrixIndex]}_{j}.png', dpi = 300)
        plt.figure()

for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for j in range(len(normalizeFunctions)):
        for jj in range(len(normalizeFunctions)):
            plt.plot(range(3, M + 1), y[attributeMatrixIndex, 1, j, 3:M + 1, jj])
        plt.legend(normalizeFunctionNames)
        plt.title(f'RDifference_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}')
        plt.savefig(f'normalizeComparision/RDifference_{attributeMatrixNames[attributeMatrixIndex]}_{j}.png', dpi = 300)
        plt.figure()

for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for j in range(len(normalizeFunctions)):
        for jj in range(len(normalizeFunctions)):
            plt.plot(range(3, M + 1), y[attributeMatrixIndex, 2, j, 3:M + 1, jj])
        plt.legend(normalizeFunctionNames)
        plt.title(f'QDifference_{attributeMatrixNames[attributeMatrixIndex]}_{normalizeFunctionNames[j]}')
        plt.savefig(f'normalizeComparision/QDifference_{attributeMatrixNames[attributeMatrixIndex]}_{j}.png', dpi = 300)
        plt.figure()

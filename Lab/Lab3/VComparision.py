import numpy as np
from matplotlib import pyplot as plt

from Vikor import vikor
from Vikor import CriteriumType
from NormalizeFunctions import noNormalize


N = 10
M = 100
loops = 1000

weights = np.random.random(N)
weights = weights / np.sum(weights)

v = np.linspace(0, 1, 11)

criteriumTypes = [CriteriumType(value) for _, value in enumerate(np.random.randint(1, 3, N))]

attributeMatrix = np.zeros((2, M, N))
S = np.zeros((attributeMatrix.shape[0], len(v), M))
R = np.zeros((attributeMatrix.shape[0], len(v), M))
Q = np.zeros((attributeMatrix.shape[0], len(v), M))
# 1. Attribute matrix - 2
# 2. v - 0-1
# 3. S, R, Q - 3
# 4. normalizeFunction vs
# 5. M - 3-100
# 6.                   vs normalizeFunction 
y = np.zeros((attributeMatrix.shape[0], len(v), 2, M + 1))

randomMaxs = np.array([4**x for x in range(N)])
    
for _ in range(loops):
    attributeMatrix[0, :, :] = np.random.uniform(np.zeros_like(randomMaxs), randomMaxs, (M, N))
    attributeMatrix[1, :, :] = np.maximum(np.random.normal(0.5 * randomMaxs, 0.5 * randomMaxs, (M, N)), 0)
    
    for k in range(3, M + 1):
        for vIndex, vValue in enumerate(v):
            for attributeMatrixIndex in range(attributeMatrix.shape[0]):
                S[attributeMatrixIndex, vIndex, :k], R[attributeMatrixIndex, vIndex, :k], Q[attributeMatrixIndex, vIndex, :k], _ = vikor(attributeMatrix[attributeMatrixIndex, :k, :], weights, criteriumTypes, noNormalize, vValue)
            
        for attributeMatrixIndex in range(attributeMatrix.shape[0]):
            for vIndex, vValue in enumerate(v):
                y[attributeMatrixIndex, vIndex, 0, k] += np.mean(np.abs(S[attributeMatrixIndex, vIndex, :k] - Q[attributeMatrixIndex, vIndex, :k]))
                y[attributeMatrixIndex, vIndex, 1, k] += np.mean(np.abs(R[attributeMatrixIndex, vIndex, :k] - Q[attributeMatrixIndex, vIndex, :k]))


y /= loops

attributeMatrixNames = ["uniform", "normal"]

legend = ["S", "R"]
legend.extend(['Q {0:1.1f}'.format(x) for x in v])
for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for vIndex, vValue in enumerate(v):
        plt.plot(range(3, M + 1), y[attributeMatrixIndex, vIndex, 0, 3:M + 1])
    plt.legend(['Q {0:1.1f}'.format(x) for x in v])
    plt.title(f'Q - S_{attributeMatrixNames[attributeMatrixIndex]}')
    plt.savefig(f'VComparision/QS_{attributeMatrixNames[attributeMatrixIndex]}.png', dpi = 300)
    plt.figure()

for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for vIndex, vValue in enumerate(v):
        plt.plot(range(3, M + 1), y[attributeMatrixIndex, vIndex, 1, 3:M + 1])
    plt.legend(['Q {0:1.1f}'.format(x) for x in v])
    plt.title(f'Q - R_{attributeMatrixNames[attributeMatrixIndex]}')
    plt.savefig(f'VComparision/QR_{attributeMatrixNames[attributeMatrixIndex]}.png', dpi = 300)
    plt.figure()
    # plt.show()

for attributeMatrixIndex in range(attributeMatrix.shape[0]): 
    for vIndex, vValue in enumerate(v):
        plt.plot(range(M), S[attributeMatrixIndex, 0, :])
        plt.plot(range(M), R[attributeMatrixIndex, 0, :])
        plt.plot(range(M), Q[attributeMatrixIndex, vIndex, :])
        plt.ylim([0, 1])
        plt.legend(["S", "R", f'Q {vValue:1.1f}'])
        plt.title(f'SRQ_{attributeMatrixNames[attributeMatrixIndex]}_{vValue:1.1f}')
        plt.savefig(f'VComparision/SRQ_{attributeMatrixNames[attributeMatrixIndex]}_{vIndex}.png', dpi = 300)
        plt.figure()
    # plt.show()
        
